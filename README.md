# Varbase AI

Provides a collection of recipes for AI tools, empowering editorial teams with a wide range of advanced Artificial Intelligence (AI) capabilities.

Enables seamless management of custom AI integrations for Varbase and supports updatable AI workflows.


## Use With [Varbase](https://www.drupal.org/project/varbase) Distribution:
This module is best used with [Varbase](https://www.drupal.org/project/varbase) distribution.

Can be installed with any Drupal 10 site.
 Even if installed with the Minimal or Standard profile.
However, using it with [Varbase](https://www.drupal.org/project/varbase) gives you way much more cool stuff!

## [Varbase documentation](https://docs.varbase.vardot.com/dev-docs/understanding-varbase/optional-components/varbase-api)
Check out Varbase documentation for more details.

Join Our Slack Team for Feedback and Support
http://varbase.slack.com

This module is sponsored and developed by [Vardot](https://www.drupal.org/vardot).
